package com.bjpowernode.crm.commons.contants;

public class Contants {
    public static final String OBJECT_RETURN_SUCCESS = "1";
    public static final String OBJECT_RETURN_FAIL = "0";
    public static final String SESSION_KEY = "sessionUser";
    public static final String REMARK_EDIT_FLAG_NO_EDITED = "0";
    public static final String REMARK_EDIT_FLAG_YES_EDITED = "1";
}
