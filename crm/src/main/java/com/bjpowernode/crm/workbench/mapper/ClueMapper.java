package com.bjpowernode.crm.workbench.mapper;

import com.bjpowernode.crm.workbench.domain.Clue;

import java.util.List;
import java.util.Map;

public interface ClueMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_clue
     *
     * @mbggenerated Wed Oct 05 23:15:22 CST 2022
     */
    int deleteByPrimaryKey(String id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_clue
     *
     * @mbggenerated Wed Oct 05 23:15:22 CST 2022
     */
    int insert(Clue record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_clue
     *
     * @mbggenerated Wed Oct 05 23:15:22 CST 2022
     */
    int insertSelective(Clue record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_clue
     *
     * @mbggenerated Wed Oct 05 23:15:22 CST 2022
     */
    Clue selectByPrimaryKey(String id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_clue
     *
     * @mbggenerated Wed Oct 05 23:15:22 CST 2022
     */
    int updateByPrimaryKeySelective(Clue record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_clue
     *
     * @mbggenerated Wed Oct 05 23:15:22 CST 2022
     */
    int updateByPrimaryKey(Clue record);

    /**
     * 保存创建的线索
     * @param clue
     * @return
     */
    int insertClue(Clue clue);
    /*
    * 按条件查询的线索
    * */
    List<Clue> selectCluesByConditionForPage(Map<String,Object> map);
    /*
     * 查询所有符合条件的所有记录条数
     * */
    int selectCountOfClueByCondition(Map<String,Object> map);
    /**
     * 根据id查询线索的明细信息
     * @param id
     * @return
     */
    Clue selectClueForDetailById(String id);
    /**
     * 根据id查询线索的信息
     * @param id
     * @return
     */
    Clue selectClueById(String id);

    /**
     * 根据id删除线索
     * @param id
     * @return
     */
    int deleteClueById(String id);
    /*
    * 根据id查询线索详细内容
    * */
    Clue selectClueDetailById(String id);
    /*
    * 更新线索表单
    * */
    int updateClue(Clue clue);

    int deleteClueByIds(String[] ids);
}