package com.bjpowernode.crm.workbench.service;

import com.bjpowernode.crm.workbench.domain.Customer;

import javax.management.Query;
import java.util.List;
import java.util.Map;

public interface CustomerService {

 List<String> queryCustomerNameByName(String name);

 List<Customer> queryCustomerByConditionForPage(Map<String,Object> map);

 int queryCountOfCustomer(Map<String,Object> map);
}
