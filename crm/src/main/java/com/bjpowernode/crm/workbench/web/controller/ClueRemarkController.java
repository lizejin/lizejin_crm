package com.bjpowernode.crm.workbench.web.controller;

import com.bjpowernode.crm.commons.contants.Contants;
import com.bjpowernode.crm.commons.domain.ReturnObject;
import com.bjpowernode.crm.commons.utils.DateFormatUtils;
import com.bjpowernode.crm.commons.utils.GetUUID;
import com.bjpowernode.crm.settings.domain.User;
import com.bjpowernode.crm.workbench.domain.ClueRemark;
import com.bjpowernode.crm.workbench.service.ClueRemarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.Date;

@Controller
public class ClueRemarkController {
    @Autowired
    private ClueRemarkService clueRemarkService;
    @RequestMapping("/workbench/clue/saveClueRemark.do")
    public @ResponseBody Object saveClueRemark(ClueRemark clueRemark, HttpSession session){
        User user = (User) session.getAttribute(Contants.SESSION_KEY);
        clueRemark.setId(GetUUID.getUUID());
        clueRemark.setCreateBy(user.getId());
        clueRemark.setCreateTime(DateFormatUtils.DateFormat("yyyy:MM:dd HH:mm:ss",new Date()));
        clueRemark.setEditFlag(Contants.REMARK_EDIT_FLAG_NO_EDITED);
        ReturnObject ret = new ReturnObject();
        try {
            int res = clueRemarkService.saveClueRemarkByClueIdAndContent(clueRemark);
            if(res>0){
                ret.setCode(Contants.OBJECT_RETURN_SUCCESS);
                ret.setOthersObj(clueRemark);
            }else{
                ret.setCode(Contants.OBJECT_RETURN_FAIL);
                ret.setMessage("系统忙，请稍后重试....");
            }
        } catch (Exception e) {
            e.printStackTrace();
            ret.setCode(Contants.OBJECT_RETURN_FAIL);
            ret.setMessage("系统忙，请稍后重试....");
        }
        return ret;
    }
    @RequestMapping("/workbench/clue/deleteRemarkById.do")
    public @ResponseBody Object deleteRemarkById(String id){
        ReturnObject ret = new ReturnObject();
        try {
            int res = clueRemarkService.deleteClueRemarkById(id);
            if(res>0){
                ret.setCode(Contants.OBJECT_RETURN_SUCCESS);
            }else {
                ret.setCode(Contants.OBJECT_RETURN_FAIL);
                ret.setMessage("系统繁忙，请稍候重试...");
            }
        } catch (Exception e) {
            e.printStackTrace();
            ret.setCode(Contants.OBJECT_RETURN_FAIL);
            ret.setMessage("系统繁忙，请稍候重试...");
        }
              return ret;
    }
}
