package com.bjpowernode.crm.settings.web.controller;

import com.bjpowernode.crm.commons.contants.Contants;
import com.bjpowernode.crm.commons.domain.ReturnObject;
import com.bjpowernode.crm.commons.utils.DateFormatUtils;
import com.bjpowernode.crm.settings.domain.User;
import com.bjpowernode.crm.settings.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("/settings/qx/user/toLogin.do")
     public String toLogin(){
         return "settings/qx/user/login";
     }


    @RequestMapping("settings/qx/user/login.do")
    public @ResponseBody Object login(String loginAct, String loginPwd, String isRemPwd, HttpServletRequest request, HttpSession session, HttpServletResponse response){
        Map<String, Object> map = new HashMap<>();
        map.put("loginAct",loginAct);
        map.put("loginPwd",loginPwd);
        User user = userService.queryUserByLoginActAndPwd(map);
        String pattern = "yyyy-MM-dd HH:mm:ss";
        String now = DateFormatUtils.DateFormat(pattern, new Date());
        ReturnObject ro = new ReturnObject();
        if(user==null){
            //登录失败，账户或者密码错误
            ro.setCode(Contants.OBJECT_RETURN_FAIL);
            ro.setMessage("账户或者密码错误");
        }else if(now.compareTo(user.getExpireTime()) > 0){
            //登录失败，账户时间过期
            ro.setCode(Contants.OBJECT_RETURN_FAIL);
            ro.setMessage("账户时间过期");
        }else if("0".equals(user.getLockState())){
            //登录失败，用户账号状态已锁定
            ro.setCode(Contants.OBJECT_RETURN_FAIL);
            ro.setMessage("用户账号状态已锁定");
        }else if(!user.getAllowIps().contains(request.getRemoteAddr())){
            //登录失败，ip受限
            ro.setCode(Contants.OBJECT_RETURN_FAIL);
            ro.setMessage("ip受限");
        }else{
            //登录成功
            ro.setCode(Contants.OBJECT_RETURN_SUCCESS);
            //在session里存用户对象信息
            session.setAttribute(Contants.SESSION_KEY,user);
            if("true".equals(isRemPwd)){
                Cookie c1 = new Cookie("loginAct", user.getLoginAct());
                c1.setMaxAge(10*24*60*60);
                response.addCookie(c1);
                Cookie c2 = new Cookie("loginPwd", user.getLoginPwd());
                c2.setMaxAge(10*24*60*60);
                response.addCookie(c2);
            }else if ("false".equals(isRemPwd)){
                Cookie c1 = new Cookie("loginAct", "1");
                c1.setMaxAge(0); //cookie存活周期设为0，即cookie马上消失
                response.addCookie(c1);
                Cookie c2 = new Cookie("loginPwd", "1");
                c2.setMaxAge(0);
                response.addCookie(c2);
            }

        }
        return ro;
    }

    //退出登录功能，实现用户安全退出，防止信息泄露
    @RequestMapping("/settings/qx/user/logout.do")
    public String logout(HttpSession session,HttpServletResponse response){
        //清空cookie，防止密码被盗
        Cookie c1 = new Cookie("loginAct", null);
        c1.setMaxAge(0); //cookie存活周期设为0，即cookie马上消失
        response.addCookie(c1);
        Cookie c2 = new Cookie("loginPwd", null);
        c2.setMaxAge(0);
        response.addCookie(c2);
        //销毁session，防止用户信息丢失被盗
        session.invalidate();
        return "redirect:/";
    }


}
