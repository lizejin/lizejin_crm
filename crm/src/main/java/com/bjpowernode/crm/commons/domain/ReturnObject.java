package com.bjpowernode.crm.commons.domain;

public class ReturnObject {
    private String code;//处理成功的标记 1---成功 0---失败
    private String message;//返回处理的结果信息
    private Object othersObj; //返回其他数据

    public ReturnObject() {
    }

    public ReturnObject(String code, String message, Object othersObj) {
        this.code = code;
        this.message = message;
        this.othersObj = othersObj;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getOthersObj() {
        return othersObj;
    }

    public void setOthersObj(Object othersObj) {
        this.othersObj = othersObj;
    }
}
