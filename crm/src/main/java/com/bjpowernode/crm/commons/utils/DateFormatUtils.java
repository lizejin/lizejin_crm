package com.bjpowernode.crm.commons.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatUtils {

    public static String DateFormat(String pattern, Date date){
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        String now = sdf.format(date);
        return now;
    }
}
